// swift-tools-version:5.7
import PackageDescription
let package = Package(
 name: "IPSFramework",
 platforms: [.iOS(.v13)],
 products: [
    .library(name: "IPSFramework", targets: ["IPSFramework"]),
    .library(name: "IndoorMapSDK", targets: ["IndoorMapSDK"]),
    .library(name: "OriientSDK", targets: ["OriientSDK"])
    ],
 dependencies: [],
 targets: [
     .binaryTarget(
         name: "IPSFramework",
         url: "https://cocoapods.oriient.me/IPSFramework_dev_3/IPSFramework_8.2.2.zip",
         checksum: "2f615d4343b2b852d680e696d4f048e26ed857a1a20e24a268ece5e25c89726f"
     ),
     .binaryTarget(
         name: "IndoorMapSDK",
         url: "https://cocoapods.oriient.me/IndoorMapSDK_dev_3/IndoorMapSDK_8.2.2.zip",
         checksum: "3a5466a3b10d514c01efa043461ae9fc40c4229f2d56e56d7eb9b740c00e3f8f"
     ),
    .binaryTarget(
         name: "OriientSDK",
         url: "https://cocoapods.oriient.me/OriientSDK_dev_3/OriientSDK_8.2.2.zip",
         checksum: "964978782db83e694312d79d77b00a25d51ebe4a1c78a632e26f2a1c597558e5"
     )
 ]
)
